#ifndef _DBT_MERGESORT_H
#define _DBT_MERGESORT_H

#include <stdio.h>
#include <stdbool.h>
#include "dbtproj.h"
#include "utils.h"

record_ptr buffer_quicksort_part(block_t *buffer, unsigned char field, unsigned int blocks_num, record_ptr low, record_ptr high);
void buffer_quicksort(block_t *buffer, unsigned char field, unsigned int blocks_num, record_ptr low, record_ptr high);
void merge_segments(unsigned int segments, unsigned char field, block_t *buffer, unsigned int blocks_num, char *outfile, unsigned int *nsorted_segs, unsigned int *nios, unsigned int *pInt);
void merge_sort_ext(char *infile, unsigned char field, block_t *buffer, unsigned int nmem_blocks, char *outfile, unsigned int *nsorted_segs, unsigned int *npasses, unsigned int *nios, unsigned int *nunique);

#endif