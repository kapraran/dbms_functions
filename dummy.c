#include "dummy.h"
#include "dbtproj.h"

#define RAND_UNLIMITED 0

int randl(int limit) {
    if (limit == RAND_UNLIMITED)
        return rand();
    return rand() % limit;
}

void dummy_populate_string(char *str) {
    char charset[] = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int i, char_len = sizeof(charset) - 1, str_len = randl(STR_LENGTH - 1);

    // ensure it's at least 2 char long
    str_len = str_len > 1 ? str_len: 2;

    for (i=0; i<str_len; i++)
        str[i] = charset[randl(char_len)];

    str[i] = '\0';
}

void dummy_populate_record(record_t *record) {
    record->recid = randl(RAND_UNLIMITED);
    record->num = randl(256);
    record->valid = randl(5) < 3; // 60 % valid

    record->rdummy1 = 0;
    record->rdummy2 = 0;
    record->rdummy3 = 0;

    dummy_populate_string(record->str);
}

block_t *dummy_create_block() {
    block_t *block = (block_t *) malloc(sizeof(block_t));
    ensure(block != NULL, "Unable to allocate memory for block_t");

    block->blockid = randl(RAND_UNLIMITED);
    block->valid = randl(5) < 3; // 60 % valid
    block->nreserved = MAX_RECORDS_PER_BLOCK;

    int i;
    for (i=0; i<MAX_RECORDS_PER_BLOCK; i++)
        dummy_populate_record(&(block->entries[i]));

    return block;
}

void dummy_populate_buffer(block_t *buffer, unsigned int blocks_num) {
    unsigned int i;
    for (i=0; i<blocks_num; i++) {
        block_t *block = dummy_create_block();
        buffer[i] = *block;
        free(block);
    }
}

void dummy_create_file(char *filename, int blocks_num) {
    FILE *file = fopen(filename, "wb");
    ensure(file != NULL, "Unable to open [wb] dummy file");

    int i;
    for (i=0; i<blocks_num; i++) {
        block_t *block = dummy_create_block();
        unsigned int nios = 0;
        file_write_blocks(file, block, 1, &nios);
        free(block);
    }

    fclose(file);
}
