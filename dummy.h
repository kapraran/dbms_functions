#ifndef _DBT_DUMMY_H
#define _DBT_DUMMY_H

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include "dbtproj.h"
#include "utils.h"

void dummy_populate_string(char *str);
void dummy_populate_record(record_t *record);
block_t *dummy_create_block();
void dummy_populate_buffer(block_t *buffer, unsigned int blocks_num);
void dummy_create_file(char *filename, int blocks_num);

#endif