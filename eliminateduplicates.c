#include <stdio.h>
#include <stdbool.h>
#include "dbtproj.h"
#include "utils.h"
#include "mergesort.h"
//#include "debug.h"

void hash_insert_pos(record_t *record, unsigned char field, block_t *buffer, unsigned int blocks_num, bool unique, record_ptr *pos) {
    unsigned int hashid = hash_record(record, field, blocks_num);
    unsigned int b, r;
    for (b=hashid; true; b++) {
        if (b == blocks_num)
            b = 0;

        pos->block_i = b;

        for (r=0; r<MAX_RECORDS_PER_BLOCK; r++) {
            pos->record_i = r;

            // found an empty position
            if (r >= buffer[b].nreserved)
                return;

            if (unique) {
                record_t *record_tmp = buffer_get_record(buffer, *pos);

                // found same record
                if (record_cmp(field, record, record_tmp) == 0) {
                    pos->block_i = -1;
                    return;
                }
            }
        }

        // if loop completed
        if (b == hashid-1) {
            pos->block_i = -1;
            return;
        }
    }
}

bool hash_insert(record_t *record, unsigned char field, block_t *buffer, unsigned int blocks_num, bool unique) {
    record_ptr pos;
    hash_insert_pos(record, field, buffer, blocks_num, unique, &pos);

    if (pos.block_i >= 0) {
        buffer[pos.block_i].entries[pos.record_i] = *record;
        buffer[pos.block_i].nreserved++;

        return true;
    }

    return false;
}

void EliminateDuplicates(char *infile, unsigned char field, block_t *buffer, unsigned int nmem_blocks, char *outfile,
                         unsigned int *nunique, unsigned int *nios) {
    // delete previous output file
    if (file_exists(outfile))
        file_remove(outfile);

    unsigned int nunique_tmp = 0;
    unsigned int nios_tmp = 0;

    // open input file
    FILE *file;
    file = fopen(infile, "rb");
    ensure(file != NULL, "Cannot open infile in EliminateDuplicates");

    // file stats
    unsigned long file_size = file_get_size(file);
    unsigned int file_blocks = file_size / sizeof(block_t);
    ensure(file_blocks > 0, "infile in EliminateDuplicates is empty");

    // if the file fits in buffer then use
    // hashing to eliminate duplicates
    if (file_blocks <= nmem_blocks - 2) {
        FILE *file_out = fopen(outfile, "wb+");
        ensure(file_out != NULL, "Cannot open outfile in EliminateDuplicates");

        // clear buffer
        buffer_invalidate(buffer, nmem_blocks);

        // use last blocks as tmp and to read from file
        block_t *block_file = &buffer[nmem_blocks - 1];
        block_t *block_tmp = &buffer[nmem_blocks - 2];
        block_tmp->nreserved = 0;
        block_tmp->valid = true;

        unsigned int b, r;
        for (b=0; b<file_blocks; b++) {
            file_read_blocks(file, block_file, 1, &nios_tmp);

            // put valid records first
            block_format(block_file);

            for (r=0; r<block_file->nreserved; r++) {
                record_t *record = &(block_file->entries[r]);

                // try to insert record if it is unique
                if (hash_insert(record, field, buffer, nmem_blocks - 2, true)) {
                    // if it is inserted then write it to output
                    block_tmp->entries[block_tmp->nreserved++] = *record;
                    nunique_tmp++;

                    if (block_tmp->nreserved >= MAX_RECORDS_PER_BLOCK) {
                        file_write_blocks(file_out, block_tmp, 1, &nios_tmp);

                        buffer_invalidate(block_tmp, 1);
                        block_tmp->valid = true;
                    }
                }
            }
        }

        // write remaining records
        if (block_tmp->nreserved > 0)
            file_write_blocks(file_out, block_tmp, 1, &nios_tmp);

        fclose(file_out);

        *nunique = nunique_tmp;
        *nios = nios_tmp;
    } else {
        // if it is larger than buffer then use mergesort
        // with modifications to avoid writing duplicates
        // in the resulting file
        merge_sort_ext(infile, field, buffer, nmem_blocks, outfile, NULL, NULL, nios, nunique);
    }

    fclose(file);
}


