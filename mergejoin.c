#include <stdio.h>
#include <stdbool.h>
#include "dbtproj.h"
#include "utils.h"
#include "mergesort.h"
//#include "debug.h"

bool records_join(unsigned char field, merge_ptr *minf, merge_ptr *maxf, block_t *block_tmp, FILE *file_out, unsigned int *nres, unsigned int *nios) {
    merge_ptr minf_copy = *minf;
    long int minf_fpos_or = ftell(minf->file);

    record_t *record_minf = buffer_get_record(minf->buffer, minf->index);
    record_t *record_maxf = buffer_get_record(maxf->buffer, maxf->index);

    // merge maxf record with each minf equal record
    while (record_cmp(field, record_minf, record_maxf) == 0) {
        record_t record_tmp = *record_minf;
        record_tmp.rdummy1 = record_maxf->recid;
        record_tmp.rdummy2 = record_maxf->num;

        block_tmp->entries[block_tmp->nreserved++] = record_tmp;
        (*nres)++;

        if (block_tmp->nreserved >= MAX_RECORDS_PER_BLOCK) {
            file_write_blocks(file_out, block_tmp, 1, nios);

            buffer_invalidate(block_tmp, 1);
            block_tmp->valid = true;
        }

        if (!merge_ptr_inc(minf, nios))
            break;

        record_minf = buffer_get_record(minf->buffer, minf->index);
    }

    // check if minf needs restoration
    if (minf_copy.file_rem_blocks > minf->file_rem_blocks) {
        long int minf_pos = minf_fpos_or - (long int) (minf_copy.buffer_curr_blocks * sizeof(block_t));
        fseek(minf_copy.file, minf_pos, SEEK_SET);

        file_read_blocks(minf_copy.file, minf_copy.buffer, minf_copy.buffer_curr_blocks, nios);
    }

    // restore original pointer data
    *minf = minf_copy;

    return merge_ptr_inc(maxf, nios);
}

void MergeJoin(char *infile1, char *infile2, unsigned char field, block_t *buffer, unsigned int nmem_blocks, char *outfile, unsigned int *nres, unsigned int *nios) {
    // delete previous output file
    if (file_exists(outfile))
        file_remove(outfile);

    ensure(nmem_blocks >= 3, "There must be at least 3 blocks of memory");

    FILE *file_out = fopen(outfile, "wb+");
    ensure(file_out != NULL, "Cannot open outfile");

    // use last block as temporary storage for write blocks
    block_t *block_tmp = &buffer[nmem_blocks-1];
    unsigned int mem_size = nmem_blocks - 1;

    // filenames for temporary files, if needed
    char filename_tmp1[256];
    char filename_tmp2[256];
    filename_tmp(filename_tmp1, outfile, ".fsrt.0.tmp");
    filename_tmp(filename_tmp2, outfile, ".fsrt.1.tmp");

    unsigned int nres_tmp = 0;
    unsigned int nios_tmp = 0;
    unsigned int nsorted_segs_tmp = 0;
    unsigned int npasses_tmp = 0;

    merge_ptr file1_mptr, file2_mptr, *minf, *maxf;
    merge_ptr_init(&file1_mptr, infile1, NULL, 0, &nios_tmp);
    merge_ptr_init(&file2_mptr, infile2, NULL, 0, &nios_tmp);

    // check if files are empty and clean the buffer
    ensure(file1_mptr.file_blocks > 0 && file2_mptr.file_blocks > 0, "Empty file as input");

    // assign min-max files
    minf = (file1_mptr.file_blocks < file2_mptr.file_blocks) ? &file1_mptr: &file2_mptr;
    maxf = (minf == &file2_mptr) ? &file1_mptr: &file2_mptr;
    bool max_sorted = false;

    if (minf->file_blocks < mem_size) { // at least one fits
        minf->buffer = buffer;
        minf->buffer_max_blocks = minf->file_blocks;

        if (maxf->file_blocks > mem_size - minf->buffer_max_blocks) {
            fclose(maxf->file);
            MergeSort(maxf->filename, field, buffer, nmem_blocks, filename_tmp1, &nsorted_segs_tmp, &npasses_tmp, &nios_tmp);
            merge_ptr_clear(maxf);
            merge_ptr_init(maxf, filename_tmp1, NULL, 0, &nios_tmp);
            max_sorted = true;
        }

        // clean buffer
        buffer_invalidate(buffer, nmem_blocks);

        // load minf file blocks
        merge_ptr_fill(minf, &nios_tmp);

        // format minf buffer
        unsigned int valid_blocks = buffer_format_complete(minf->buffer, minf->file_blocks);
        ensure(valid_blocks > 0, "File has no valid blocks, aborting");

        unsigned int rem_blocks = mem_size - valid_blocks;
        minf->buffer_max_blocks = valid_blocks;
        minf->buffer_curr_blocks = valid_blocks;

        // sort minf buffer
        record_ptr low = { .block_i = 0, .record_i = 0 };
        record_ptr high = { .block_i= minf->buffer_max_blocks - 1, .record_i = minf->buffer[minf->buffer_max_blocks - 1].nreserved - 1 };
        buffer_quicksort(minf->buffer, field, minf->buffer_max_blocks, low, high);

        // clean the rest of the buffer
        buffer_invalidate(&buffer[minf->buffer_max_blocks], rem_blocks);

        maxf->buffer = &buffer[minf->buffer_max_blocks];
        maxf->buffer_max_blocks = rem_blocks;
        merge_ptr_fill(maxf, &nios_tmp);

        if (!max_sorted) {
            unsigned int valid_blocks = buffer_format_complete(maxf->buffer, maxf->file_blocks);
            ensure(valid_blocks > 0, "File has no valid blocks, aborting");
            maxf->buffer_max_blocks = valid_blocks;
            maxf->buffer_curr_blocks = valid_blocks;

            // sort maxf buffer
            record_ptr low = { .block_i = 0, .record_i = 0 };
            record_ptr high = { .block_i= maxf->buffer_max_blocks - 1, .record_i = maxf->buffer[maxf->buffer_max_blocks - 1].nreserved - 1 };
            buffer_quicksort(maxf->buffer, field, maxf->buffer_max_blocks, low, high);
        }
    } else { // both files bigger than the buffer
        // merge sort each file
        MergeSort(minf->filename, field, buffer, nmem_blocks, filename_tmp1, &nsorted_segs_tmp, &npasses_tmp, &nios_tmp);
        MergeSort(maxf->filename, field, buffer, nmem_blocks, filename_tmp2, &nsorted_segs_tmp, &npasses_tmp, &nios_tmp);

        // load the sorted files
        merge_ptr_clear(minf);
        merge_ptr_init(minf, filename_tmp1, buffer, mem_size - 1, &nios_tmp);
        merge_ptr_clear(maxf);
        merge_ptr_init(maxf, filename_tmp2, &buffer[minf->buffer_max_blocks], 1, &nios_tmp);
    }

    // clean temp block
    buffer_invalidate(block_tmp, 1);
    block_tmp->valid = true;

    // merge the files
    while (true) {
        record_t *record_minf = buffer_get_record(minf->buffer, minf->index);
        record_t *record_maxf = buffer_get_record(maxf->buffer, maxf->index);

        if (record_cmp(field, record_minf, record_maxf) == 0) {
            if (!records_join(field, minf, maxf, block_tmp, file_out, &nres_tmp, &nios_tmp))
                break;
        } else {
            if (record_cmp(field, record_minf, record_maxf) < 0) {
                if (!merge_ptr_inc(minf, &nios_tmp))
                    break;
            } else {
                if (!merge_ptr_inc(maxf, &nios_tmp))
                    break;
            }
        }
    }

    if (block_tmp->nreserved > 0)
        file_write_blocks(file_out, block_tmp, 1, &nios_tmp);

    merge_ptr_clear(minf);
    merge_ptr_clear(maxf);

    fclose(file_out);

    // remove temporary files
    if (file_exists(filename_tmp1))
        file_remove(filename_tmp1);

    if (file_exists(filename_tmp2))
        file_remove(filename_tmp2);

    // save stats
    *nres = nres_tmp;
    *nios = nios_tmp;
}
