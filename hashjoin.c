#include <stdio.h>
#include <stdbool.h>
#include "dbtproj.h"
#include "utils.h"
#include "mergesort.h"
#include "debug.h"

void hash_table_create(merge_ptr *mptr, unsigned char field, block_t *block_tmp, unsigned int *nios) {
    unsigned int b, r;
    for (b=0; b<mptr->file_blocks; b++) {
        // clear temporary block
        buffer_invalidate(block_tmp, 1);

        // load block
        file_read_blocks(mptr->file, block_tmp, 1, nios);

        if (!block_tmp->valid)
            continue;

        // format block
        block_format(block_tmp);

        for (r=0; r<block_tmp->nreserved; r++) {
            record_t record = block_tmp->entries[r];
            unsigned int key = hash_record(&record, field, mptr->file_blocks), pos;

            // find the position (block) to put it in
            bool checked = false;
            for (pos=key; true; pos++) {
                if (pos == mptr->file_blocks)
                    pos = 0;

                // the loop is completed
                if (pos == key) {
                    if (checked)
                        break;

                    checked = true;
                }

                // current block is full
                if (mptr->buffer[pos].nreserved >= MAX_RECORDS_PER_BLOCK) {
                    continue;
                } else {
                    mptr->buffer[pos].entries[mptr->buffer[pos].nreserved++] = record;
                    break;
                }
            }
        }
    }
}

void hash_table_find(block_t *buffer, unsigned int buffer_size, unsigned char field, record_t *record, record_ptr *index) {
    long long int key = hash_record(record, field, buffer_size), b, r;
    long long int b_start = index->block_i != -1 ? index->block_i: key;
    long long int r_start = index->block_i != -1 ? index->record_i + 1: 0;
    bool checked = b_start == key ? false: true;

    b=b_start;
    while (true) {
        // check if loop completed
        if (b == key) {
            if (checked) {
                index->block_i = -1;
                return;
            }

            checked = true;
        }

        for (r=r_start; r<buffer[b].nreserved; r++) {
            index->block_i = b;
            index->record_i = r;
            record_t *record_curr = buffer_get_record(buffer, *index);

            // return if same record is found
            if (record_cmp(field, record_curr, record) == 0)
                return;
        }

        r_start = 0;

        // if there are empty positions in this block then break
        if (buffer[b].nreserved < MAX_RECORDS_PER_BLOCK) {
            index->block_i = -1;
            return;
        }

        b++;
    }
}

void hash_table_create_file(merge_ptr *mptr, unsigned char field, block_t *buffer, unsigned int buffer_size, unsigned int *nios) {
    unsigned int i, f, b, r;
    unsigned int buckets_num = buffer_size - 1;
    block_t *block_tmp = &buffer[buckets_num];
    FILE *bucket_files[buckets_num];

    // init to NULL
    for (f=0; f<buckets_num; f++) bucket_files[f] = NULL;

    // clear buffer
    buffer_invalidate(buffer, buffer_size);
    for (i=0; i<buffer_size; i++)
        buffer[i].valid = true;

    for (b=0; b<mptr->file_blocks; b++) {
        // clear temporary block
        buffer_invalidate(block_tmp, 1);

        // load block
        file_read_blocks(mptr->file, block_tmp, 1, nios);

        if (!block_tmp->valid)
            continue;

        // format block
        block_format(block_tmp);

        for (r=0; r<block_tmp->nreserved; r++) {
            record_t record = block_tmp->entries[r];
            unsigned int key = hash_record(&record, field, buckets_num);

            // append record to bucket block
            buffer[key].entries[buffer[key].nreserved++] = record;

            // check if bucket block is full
            if (buffer[key].nreserved >= MAX_RECORDS_PER_BLOCK) {
                // open bucket file
                if (bucket_files[key] == NULL) {
                    char filename[256];
                    filename_hash_bucket(filename, mptr->filename, key);

                    bucket_files[key] = fopen(filename, "wb+");
                    ensure(bucket_files[key] != NULL, "Cannot open temporary bucket file");
                }

                // write bucket block to bucket file
                file_write_blocks(bucket_files[key], &buffer[key], 1, nios);

                // clear bucket block
                buffer_invalidate(&buffer[key], 1);
                buffer[key].valid = true;
            }
        }
    }

    // write remaining blocks
    for (i=0; i<buckets_num; i++) {
        if (buffer[i].nreserved > 0) {
            // open bucket file
            if (bucket_files[i] == NULL) {
                char filename[256];
                filename_hash_bucket(filename, mptr->filename, i);

                bucket_files[i] = fopen(filename, "wb+");
                ensure(bucket_files[i] != NULL, "Cannot open temporary bucket file");
            }

            // write bucket block to bucket file
            file_write_blocks(bucket_files[i], &buffer[i], 1, nios);
        }
    }

    // close files
    for (f=0; f<buckets_num; f++)
        if (bucket_files[f] != NULL)
            fclose(bucket_files[f]);
}


void hash_bucket_join_record(merge_ptr *minbck, record_t record, unsigned char field, block_t *block_tmp, FILE *file_out, unsigned int *nres, unsigned int *nios) {
    do {
        record_t *record_curr = buffer_get_record(minbck->buffer, minbck->index);

        if (record_cmp(field, record_curr, &record) == 0) {
            // join records
            record.rdummy1 = record_curr->recid;
            record.rdummy2 = record_curr->num;

            (*nres)++;

            // append record to output block
            block_tmp->entries[block_tmp->nreserved++] = record;

            if (block_tmp->nreserved >= MAX_RECORDS_PER_BLOCK) {
                file_write_blocks(file_out, block_tmp, 1, nios);

                buffer_invalidate(block_tmp, 1);
                block_tmp->valid = true;
            }
        }
    } while (merge_ptr_inc(minbck, nios));

    // reset minbck
    merge_ptr_reset(minbck, nios);
}

void HashJoin(char *infile1, char *infile2, unsigned char field, block_t *buffer, unsigned int nmem_blocks, char *outfile, unsigned int *nres, unsigned int *nios) {
    // delete previous output file
    file_remove(outfile);

    // check for sufficient memory
    ensure(nmem_blocks >= 3, "There must be at least 3 blocks of memory");

    // open output file
    FILE *file_out = fopen(outfile, "wb+");
    ensure(file_out != NULL, "Cannot open outfile");

    // use last block as temporary storage for write blocks
    block_t *block_tmp = &buffer[nmem_blocks-1];
    unsigned int mem_size = nmem_blocks - 1;

    // use local variables for stats
    unsigned int nres_tmp = 0;
    unsigned int nios_tmp = 0;

    // use merge_ptr helpers to open input files
    merge_ptr file1_mptr, file2_mptr, *minf, *maxf;
    merge_ptr_init(&file1_mptr, infile1, NULL, 0, &nios_tmp);
    merge_ptr_init(&file2_mptr, infile2, NULL, 0, &nios_tmp);

    // check if files are empty and clean the buffer
    ensure(file1_mptr.file_blocks > 0 && file2_mptr.file_blocks > 0, "Empty file as input");

    // assign min-max files
    minf = (file1_mptr.file_blocks < file2_mptr.file_blocks) ? &file1_mptr: &file2_mptr;
    maxf = (minf == &file2_mptr) ? &file1_mptr: &file2_mptr;

    if (minf->file_blocks < mem_size) { // at least one fits
        minf->buffer = buffer;

        // clean buffer
        buffer_invalidate(buffer, nmem_blocks);

        // create in memory hash table for minf
        hash_table_create(minf, field, block_tmp, &nios_tmp);

        // clear temporary block
        buffer_invalidate(block_tmp, 1);
        block_tmp->valid = true;

        // use the next block to read maxf contents
        block_t *block_maxf = &buffer[minf->file_blocks];

        // read each block from maxf
        unsigned int b, r;
        for (b=0; b<maxf->file_blocks; b++) {
            // clear temporary block
            buffer_invalidate(block_maxf, 1);

            // load block
            file_read_blocks(maxf->file, block_maxf, 1, &nios_tmp);

            // skip if it's invalid
            if (!block_maxf->valid)
                continue;

            // format block
            block_format(block_maxf);

            for (r=0; r<block_maxf->nreserved; r++) {
                record_t record = block_maxf->entries[r];
                record_ptr index = { .block_i = -1, .record_i = 0 };

                hash_table_find(minf->buffer, minf->file_blocks, field, &record, &index);
                while (index.block_i != -1) {
                    record_t *record_other = buffer_get_record(minf->buffer, index);

                    // join records
                    record.rdummy1 = record_other->recid;
                    record.rdummy2 = record_other->num;

                    nres_tmp++;

                    // write it to the output
                    block_tmp->entries[block_tmp->nreserved++] = record;

                    if (block_tmp->nreserved >= MAX_RECORDS_PER_BLOCK) {
                        file_write_blocks(file_out, block_tmp, 1, &nios_tmp);

                        buffer_invalidate(block_tmp, 1);
                        block_tmp->valid = true;
                    }

                    // find next record
                    hash_table_find(minf->buffer, minf->file_blocks, field, &record, &index);
                }
            }
        }

        // write remaining records
        if (block_tmp->nreserved > 0)
            file_write_blocks(file_out, block_tmp, 1, &nios_tmp);
    } else { // both files bigger than the buffer
        unsigned int buckets_num = nmem_blocks - 1, i;

        hash_table_create_file(&file1_mptr, field, buffer, nmem_blocks, &nios_tmp);
        hash_table_create_file(&file2_mptr, field, buffer, nmem_blocks, &nios_tmp);

        // clean buffer
        buffer_invalidate(buffer, nmem_blocks);
        block_tmp->valid = true;

        // join records of equivalent buckets
        for (i=0; i<buckets_num; i++) {
            char filename_b1[256], filename_b2[256];
            filename_hash_bucket(filename_b1, file1_mptr.filename, i);
            filename_hash_bucket(filename_b2, file2_mptr.filename, i);

            // skip if any of the two doesnt exist
            if (!file_exists(filename_b1) || !file_exists(filename_b2)) {
                file_remove(filename_b1);
                file_remove(filename_b2);

                continue;
            }

            // load files using merge_ptr for help
            merge_ptr bucket1_mptr, bucket2_mptr, *minbck, *maxbck;
            merge_ptr_init(&bucket1_mptr, filename_b1, NULL, 0, &nios_tmp);
            merge_ptr_init(&bucket2_mptr, filename_b2, NULL, 0, &nios_tmp);

            // set min and max file
            minbck = (bucket1_mptr.file_blocks < bucket2_mptr.file_blocks) ? &bucket1_mptr: &bucket2_mptr;
            maxbck = (minbck == &bucket2_mptr) ? &bucket1_mptr: &bucket2_mptr;

            // give mem_size - 1 blocks to the minbck and one block for maxbck
            minbck->buffer = buffer;
            minbck->buffer_max_blocks = mem_size - 1;
            merge_ptr_fill(minbck, &nios_tmp);

            maxbck->buffer = &buffer[mem_size - 1];
            maxbck->buffer_max_blocks = 1;
            merge_ptr_fill(maxbck, &nios_tmp);

            // read each block from max bucket and join with records from min bucket
            do {
                record_t record = *buffer_get_record(maxbck->buffer, maxbck->index);

                hash_bucket_join_record(minbck, record, field, block_tmp, file_out, &nres_tmp, &nios_tmp);
            } while (merge_ptr_inc(maxbck, &nios_tmp));

            // clear bucket files merge_ptr helpers
            merge_ptr_clear(&bucket1_mptr);
            merge_ptr_clear(&bucket2_mptr);

            // remove temp hash files
            file_remove(filename_b1);
            file_remove(filename_b2);
        }

        if (block_tmp->nreserved > 0)
            file_write_blocks(file_out, block_tmp, 1, &nios_tmp);
    }

    // clear input file merge_ptr helpers
    merge_ptr_clear(&file1_mptr);
    merge_ptr_clear(&file2_mptr);

    // close output file
    fclose(file_out);

    // save stats
    *nres = nres_tmp;
    *nios = nios_tmp;
}