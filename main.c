#include <stdio.h>
#include <stdbool.h>
#include "dbtproj.h"
#include "dummy.h"
//#include "debug.h"

int main() {
    unsigned int blocks_num = 64;
    block_t buffer[blocks_num];
    char filename1[256];
    char filename2[256];
    char filename_out[256];

    // TODO change filenames
    strcpy(filename1, "D:\\dbt_files\\content1.bin");
    strcpy(filename2, "D:\\dbt_files\\content2.bin");
    strcpy(filename_out, "D:\\dbt_files\\result.bin");

    dummy_create_file(filename1, blocks_num * 120 + 4);
    dummy_create_file(filename2, blocks_num * 40);

    unsigned char field = 1;
    unsigned int nsorted_segs = 0;
    unsigned int npasses = 0;
    unsigned int nios = 0;
    unsigned int nunique = 0;
    unsigned int nres = 0;

    MergeSort(filename1, field, buffer, blocks_num, filename_out, &nsorted_segs, &npasses, &nios);
//    EliminateDuplicates(filename1, field, buffer, blocks_num, filename_out, &nunique, &nios);
//    MergeJoin(filename1, filename2, field, buffer, blocks_num, filename_out, &nres, &nios);
//    HashJoin (filename1, filename2, field, buffer, blocks_num, filename_out, &nres, &nios);

    printf("[main.c] nsorted_segs=%u npasses=%u nios=%u nunique=%u nres=%u\n", nsorted_segs, npasses, nios, nunique, nres);

    return 0;
}