#include "utils.h"
#include "dbtproj.h"

//const char FILENAME_PREFIX[] = "C:\\Users\\nikos\\Desktop\\dbt\\";

/**
 * Ensures that {pass} equals true otherwise it prints
 * the {message} and the program exits
 */
 void ensure(bool pass, char *message) {
    if (!pass) {
        printf("ERROR: %s\n", message);
        exit(1);
    }
}

void filename_tmp(char *filename, char *base, char *filename_rel) {
    sprintf(filename, "%s%s", base, filename_rel);
}

void filename_segment(char *filename, char *base, unsigned int rep, unsigned int index) {
    sprintf(filename, "%s.seg.%d.%d.tmp", base, rep, index);
}

void filename_hash_bucket(char *filename, char *base, unsigned int index) {
    sprintf(filename, "%s.buc.%d.tmp", base, index);
}

void filename_segment_buffer(char *filename, char *base, unsigned int index) {
    filename_segment(filename, base, 0, index);
}

/**
 *  Returns the size of {file}
 */
unsigned long file_get_size(FILE *file) {
    unsigned long pos = ftell(file);
    fseek(file, 0, SEEK_END);
    unsigned long size = ftell(file);
    fseek(file, pos, SEEK_SET);

    return size;
}

void file_read_blocks(FILE *file, block_t *buffer, unsigned int blocks_num, unsigned int *nios) {
    size_t count = fread(buffer, sizeof(block_t), blocks_num, file);
    ensure(count > 0, "Cannot read blocks from file");

    (*nios) += blocks_num;
}

void file_write_blocks(FILE *file, block_t *buffer, unsigned int blocks_num, unsigned int *nios) {
    size_t count = fwrite(buffer, sizeof(block_t), blocks_num, file);
    ensure(count > 0, "Cannot write block to file");

    (*nios) += blocks_num;
}

void file_write_buffer(block_t *buffer, unsigned int blocks_num, unsigned int id, char *outfile, unsigned int *nsorted_segs, unsigned int *nios) {
    char filename[256];
    filename_segment_buffer(filename, outfile, id);

    FILE *file_tmp = fopen(filename, "wb+");
    file_write_blocks(file_tmp, buffer, blocks_num, nios);
    fclose(file_tmp);

    (*nsorted_segs)++;
}

/**
 *  Deletes the file
 */
void file_remove(char *filename) {
    if (!file_exists(filename))
        return;

    char error_msg[256];
    sprintf(error_msg, "Cannot remove file '%s'", filename);

    ensure(remove(filename) == 0, error_msg);
}

/**
 * Checks if file exists
 */
bool file_exists(char *filename) {
    return access(filename, F_OK) != -1 ? true: false;
}

/**
 * Increments the record pointer by one
 */
bool record_ptr_inc(block_t *buffer, int blocks_num, record_ptr *index) {
    index->record_i++;

    // check if its out of bounds
    if (index->block_i >= blocks_num || index->record_i < 0)
        return false;

    if (index->block_i < 0 && index->record_i == 0) {
        index->block_i = 0;
        return true;
    }

    if (index->record_i >= buffer[index->block_i].nreserved) {
        index->block_i++;
        index->record_i = 0;

        if (index->block_i >= blocks_num)
            return false;
    }

    return true;
}

bool record_ptr_dec(block_t *buffer, int blocks_num, record_ptr *index) {
    index->record_i--;

    // check if its out of bounds
    if (index->block_i < 0 || (index->block_i >= blocks_num && index->record_i >= 0))
        return false;

    if (index->record_i < 0) {
        index->block_i--;
        if (index->block_i < 0)
            return false;

        index->record_i = buffer[index->block_i].nreserved - 1;
    }

    return true;
}

int record_ptr_cmp(record_ptr a, record_ptr b) {
    long long int p1 = a.block_i * MAX_RECORDS_PER_BLOCK + a.record_i;
    long long int p2 = b.block_i * MAX_RECORDS_PER_BLOCK + b.record_i;

    if (p1 == p2)
        return 0;

    return p1 > p2 ? 1: -1;
}

void record_swap(record_t *a, record_t *b) {
    record_t tmp = *a;
    *a = *b;
    *b = tmp;
}

int record_cmp(unsigned char field, record_t *record_a, record_t *record_b) {
    switch (field) {
        case REC_CMP_RECID:
            if (record_a->recid == record_b->recid)
                return 0;
            return record_a->recid > record_b->recid ? 1: -1;

        case REC_CMP_NUM:
            if (record_a->num == record_b->num)
                return 0;
            return record_a->num > record_b->num ? 1: -1;

        case REC_CMP_STR:
            return strcmp(record_a->str, record_b->str);
        
        case REC_CMP_NUM_STR:
            if (record_a->num == record_b->num)
                return strcmp(record_a->str, record_b->str);
            return record_a->num > record_b->num ? 1: -1;

        default: // error
            return -2;
    }
}

void record_invalidate(record_t *record) {
    record->recid = 0;
    record->num = 0;
    record->str[0] = '\0';
    record->valid = false;
    record->rdummy1 = 0;
    record->rdummy2 = 0;
    record->rdummy3 = 0;
}

unsigned int buffer_format(block_t *buffer, unsigned int blocks_num) {
    unsigned int low = 0, high = blocks_num - 1;

    while (low <= high) {
        if (!buffer[low].valid) {
            while (!buffer[high].valid && high > 0)
                high--;

            if (low >= high)
                return low;

            block_swap(&(buffer[low]), &(buffer[high]));
        }

        low++;
    }

    return low;
}

unsigned int buffer_format_complete(block_t *buffer, unsigned int blocks_num) {
    unsigned int valid_blocks = buffer_format(buffer, blocks_num);

    // skip if there are no valid blocks
    if (valid_blocks == 0)
        return 0;

    block_t block_tmp;
    buffer_invalidate(&block_tmp, 1);
    block_tmp.valid = true;

    unsigned int valid_blocks_new = 0;
    int b, r;
    for (b=0; b<valid_blocks; b++) {
        block_format(&buffer[b]);
        for (r=0; r<buffer[b].nreserved; r++) {
            block_tmp.entries[block_tmp.nreserved] = buffer[b].entries[r];
            block_tmp.nreserved++;

            if (block_tmp.nreserved >= MAX_RECORDS_PER_BLOCK) {
                buffer[valid_blocks_new] = block_tmp;
                valid_blocks_new++;

                buffer_invalidate(&block_tmp, 1);
                block_tmp.valid = true;
            }
        }
    }

    if (block_tmp.nreserved > 0) {
        buffer[valid_blocks_new] = block_tmp;
        valid_blocks_new++;
    }

    return valid_blocks_new;
}

record_t *buffer_get_record(block_t *buffer, record_ptr index) {
    return &(buffer[index.block_i].entries[index.record_i]);
}

void buffer_invalidate(block_t *buffer, unsigned int blocks_num) {
    int i, j;
    for (i=0; i<blocks_num; i++) {
        buffer[i].blockid = 0;
        buffer[i].nreserved = 0;
        buffer[i].valid = false;
        buffer[i].misc = '\0';
        buffer[i].bdummy1 = 0;
        buffer[i].bdummy2 = 0;
        buffer[i].bdummy3 = 0;

        for (j=0; j<MAX_RECORDS_PER_BLOCK; j++)
            record_invalidate(&(buffer[i].entries[j]));
    }
}

void buffer_eliminate_duplicates(block_t *buffer, unsigned char field, unsigned int *blocks_num, unsigned int *nunique) {
    block_t block_tmp;
    buffer_invalidate(&block_tmp, 1);
    block_tmp.valid = true;
    record_t record_tmp;
    bool f_empty = true;
    unsigned int blocks_num_new = 0;

    unsigned int b, r;
    for (b=0; b<(*blocks_num); b++) {
        for (r=0; r<buffer[b].nreserved; r++) {
            if (f_empty) {
                record_tmp = buffer[b].entries[r];
                block_tmp.entries[block_tmp.nreserved++] = record_tmp;
                (*nunique)++;

                f_empty = false;
                continue;
            }

            // check if current record is same with previous
            if (record_cmp(field, &(buffer[b].entries[r]), &record_tmp) != 0) {
                record_tmp = buffer[b].entries[r];
                block_tmp.entries[block_tmp.nreserved++] = record_tmp;
                (*nunique)++;

                // replace previous block with the new one
                if (block_tmp.nreserved >= MAX_RECORDS_PER_BLOCK) {
                    buffer[blocks_num_new++] = block_tmp;

                    buffer_invalidate(&block_tmp, 1);
                    block_tmp.valid = true;
                }
            }
        }
    }

    if (block_tmp.nreserved > 0)
        buffer[blocks_num_new++] = block_tmp;

    *blocks_num = blocks_num_new;
}

void block_swap(block_t *a, block_t *b) {
    block_t tmp = *a;
    *a = *b;
    *b = tmp;
}

void block_format(block_t *block) {
    unsigned int low = 0, high = MAX_RECORDS_PER_BLOCK;

    while (low <= high) {
        if (!block->entries[low].valid) {
            while (!block->entries[high].valid && high > 0)
                high--;

            if (low >= high)
                break;

            record_swap(&(block->entries[low]), &(block->entries[high]));
        }

        low++;
    }

    block->nreserved = low;
}

/**
 * fills a merge pointer with remaining data that can fit it's buffer
 * if there are no blocks to load, it returns false
 */
bool merge_ptr_fill(merge_ptr *ptr, unsigned int *nios) {
    if (ptr->file_rem_blocks <= 0) {
        ptr->buffer_curr_blocks = 0;
        return false;
    }

    unsigned int blocks_num = (ptr->file_rem_blocks < ptr->buffer_max_blocks) ? ptr->file_rem_blocks : ptr->buffer_max_blocks;
    ptr->file_rem_blocks -= blocks_num;

    file_read_blocks(ptr->file, ptr->buffer, blocks_num, nios);

    ptr->buffer_curr_blocks = blocks_num;
    return true;
}

/**
 * clears a merge pointer
 */
void merge_ptr_clear(merge_ptr *ptr) {
    fclose(ptr->file);
    ptr->filename[0] = '\0';
    ptr->file_blocks = 0;
    ptr->file_rem_blocks = 0;
    ptr->buffer = NULL;
    ptr->index.block_i = 0;
    ptr->index.record_i = 0;
    ptr->buffer_max_blocks = 0;
    ptr->buffer_curr_blocks = 0;
}

/**
 * resets a merge pointer
 */
void merge_ptr_reset(merge_ptr *ptr, unsigned int *nios) {
    ptr->index.block_i = 0;
    ptr->index.record_i = 0;

    if (ptr->file_blocks > ptr->buffer_max_blocks) {
        fseek(ptr->file, 0, SEEK_SET);
        ptr->file_rem_blocks = ptr->file_blocks;
        ptr->buffer_curr_blocks = 0;

        merge_ptr_fill(ptr, nios);
    } else {
        ptr->file_rem_blocks = 0;
        ptr->buffer_curr_blocks = ptr->file_blocks;
    }
}

/**
 * initializes a merge pointer
 */
bool merge_ptr_init(merge_ptr *ptr, char *filename, block_t *buffer, unsigned int blocks_num, unsigned int *nios) {
    ptr->file = fopen(filename, "rb");
    ensure(ptr->file != NULL, "Cannot open file");

    strcpy(ptr->filename, filename);
    ptr->file_blocks = file_get_size(ptr->file) / sizeof(block_t);
    ptr->file_rem_blocks = ptr->file_blocks;
    ptr->buffer = buffer;
    ptr->index.block_i = 0;
    ptr->index.record_i = 0;
    ptr->buffer_max_blocks = blocks_num;
    ptr->buffer_curr_blocks = 0;

    if (blocks_num == 0)
        return false;

    if (!merge_ptr_fill(ptr, nios)) {
        merge_ptr_clear(ptr);
        return false;
    }

    return true;
}

/**
 * increments merge_ptr's index
 */
bool merge_ptr_inc(merge_ptr *ptr, unsigned int *nios) {
    if (!record_ptr_inc(ptr->buffer, ptr->buffer_curr_blocks, &(ptr->index))) {
        ptr->index.block_i = 0;

        return merge_ptr_fill(ptr, nios);
    }

    return true;
}

/**
 * returns a record based on merge_ptr's index
 */
record_t *merge_ptr_get_record(merge_ptr *ptr) {
    return buffer_get_record(ptr->buffer, ptr->index); 
}


unsigned int hash_int(unsigned int x) {
    x = ((x >> 16) ^ x) * 0x45d9f3b;
    x = ((x >> 16) ^ x) * 0x45d9f3b;
    x = (x >> 16) ^ x;
    return x;
}

/**
 * returns an interger hash for a record based on the field
 */
unsigned int hash_record(record_t *record, unsigned char field, unsigned int limit) {
    unsigned int i, sum = 0;

    switch (field) {
        case REC_CMP_RECID:
            return hash_int(record->recid) % limit;

        case REC_CMP_NUM:
            return hash_int(record->num) % limit;

        case REC_CMP_STR:
            for (i=0; strlen(record->str); i++)
                sum += record->str[i];
            return hash_int(sum) % limit;

        case REC_CMP_NUM_STR:
            for (i=0; strlen(record->str); i++)
                sum += record->str[i];
            return hash_int(record->num + sum) % limit;

        default: // error
            return 0;
    }
}
