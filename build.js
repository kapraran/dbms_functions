var execa = require('execa');
var program = require('commander');
var chalk = require('chalk');

program
  .version('0.1.0')
  .option('-r, --run', 'Run program after compilation')
  .parse(process.argv);

var exe = 'dbtproj';

function print(str) {
    process.stdout.write(str);
}

function printCommand(com) {
    print('$ ' + com + ' ');
}

function println(str) {
    console.log(str);
}

function printGccPretty(name, output) {
    if (output.length < 1)
        return;

    console.log('--[%s]--', name);
    var indent = chalk.gray('\n  >  ');

    output = indent + output.replace(/\n|\r\n/g, indent);
    output = output.replace(/warning\:/g, chalk.yellow('{warning}'));
    output = output.replace(/error\:/g, chalk.red('{error}'));
    console.log(output);
}

function main() {
    var sources = ['main.c', 'dummy.c', 'utils.c', 'mergesort.c', 'eliminateduplicates.c'];
    var args = ['-o', exe].concat(sources);

    printCommand('gcc');
    execa('gcc', args)
        .then(function(result) {
            println(chalk.green('✔'));

            printGccPretty('stdout', result.stdout);
            printGccPretty('stderr', result.stderr);

            if (program.run) {
                printCommand(exe);
                var proc = execa(exe);

                proc.stdout.pipe(process.stdout);
                proc.stderr.pipe(process.stderr);

                return proc;
            }
        })
        .then(function(result) {
            if (!program.run)
                return;

            println(chalk.bgGreen.black(' completed '));
        })
        .catch(function(error) {
             println(chalk.red('✖'));
             printGccPretty('stdout', error.stdout);
             printGccPretty('stderr', error.stderr);
        });
}

if (require.main === module)
    main();