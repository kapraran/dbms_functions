#ifndef _DBT_DEBUG_H
#define _DBT_DEBUG_H

#include <stdio.h>
#include "dbtproj.h"
#include "dummy.h"
#include "utils.h"

void debug_record(record_t *record) {
    printf("[record_t recid:%6d num:%6d valid:%d rd1:%d rd2:%d str(%d):\"%s\"]\n", record->recid, record->num, record->valid, record->rdummy1, record->rdummy2, strlen(record->str), record->str);
}

void debug_block(block_t *block, bool all_records) {
    printf("[block v:%d nr:%3d]\n", block->valid, block->nreserved);
    int records_num = all_records ? MAX_RECORDS_PER_BLOCK: block->nreserved;
    int r;
    for (r=0; r<records_num; r++)
        debug_record(&(block->entries[r]));
}

void debug_buffer(block_t *buffer, unsigned int blocks_num, bool all_records) {
    printf("--debug_buffer bn:%d--\n", blocks_num);
    int b, r;
    for (b=0; b<blocks_num; b++)
        debug_block(&buffer[b], all_records);
}

void debug_file(char *filename) {
    if (!file_exists(filename))
        return;

    printf("==debug_file '%s'==========\n", filename);
    FILE *file = fopen(filename, "rb");
    int file_size = file_get_size(file);
    int file_blocks = file_size / sizeof(block_t);
    int b;
    for (b=0; b<file_blocks; b++) {
        block_t block;
        fread(&block, sizeof(block_t), 1, file);
        printf("#%d ", b);
        debug_block(&block, true);
    }

    fclose(file);
}

void debug_record_ptr(record_ptr ptr) {
    printf("[record_ptr %d %d]\n", ptr.block_i, ptr.record_i);
}

void block_debug(block_t *block) {
    printf("[BLOCK] v:%d i:%d r:%d ", block->valid, block->blockid, block->nreserved);
    printf("(dummy '%c', %d, %d, %d)\n", block->misc, block->bdummy1, block->bdummy2, block->bdummy3);

    int i;
    for (i=0; i<MAX_RECORDS_PER_BLOCK; i++)
        debug_record(&(block->entries[i]));
    }

/** BUFFER FORMAT **/
unsigned int buffer_count_valid(block_t *buffer, unsigned int size) {
    unsigned int i, count = 0;
    for (i=0; i<size; i++)
        if (buffer[i].valid) count++;
    
    return count;
}

void debug_buffer_format() {
    unsigned int k, i, j, count = 128, orig_valid;
    for (k=0; k<=100; k++) {
        for (j=0; j<=20; j++) {
            block_t buffer[count];
            for (i=0; i<count; i++) {
                block_t *b = dummy_create_block();
                b->valid = rand() % 100 < k;
                buffer[i] = *b;
                free(b);
            }

            orig_valid = buffer_count_valid(buffer, count);
            printf("--debug #%d--", k);
            for (i=0; i<count; i++)
                block_debug(&(buffer[i]));

            unsigned int vb = buffer_format(buffer, count);

            if (vb != orig_valid) {
                printf("FAILED k=%d j=%d\n", k, j);
                printf("vb:%d ov:%d\n", vb, orig_valid);
                for (i=0; i<count; i++) {
                    printf("[%d]", i);
                    block_debug(&(buffer[i]));
                }
                exit(1);
            }
        }
    }
}

/** block_format **/
void debug_block_format() {
    block_t *block = dummy_create_block();

    int i,j,k;
    for (i=0; i<=100; i++) {
        for (j=0; j<20; j++) {
            printf("-nxt-\n");

            int orig_valid = 0;
            for (k=0; k<MAX_RECORDS_PER_BLOCK; k++) {
                block->entries[k].valid = rand() % 100 < i;

                if (block->entries[k].valid)
                    orig_valid++;

                debug_record(&(block->entries[k]));
            }

            block_format(block);

            if (block->nreserved != orig_valid) {
                printf("[FAILED %d %d]\n", i, j);
                exit(1);
            }

            printf("[pass %d %d]\n", i, j);
        }
    }
}

/** record_ptr_inc and record_ptr_dec **/
void debug_record_ptr_inc_dec() {
    unsigned int blocks_num = 3;
    block_t buffer[blocks_num];
    dummy_populate_buffer(buffer, blocks_num);

    record_ptr index = {.block_i = 0, .record_i = 2};

    debug_record_ptr(index);

    int i;
    for (i=0; i<151; i++) {
        bool inb = record_ptr_dec(buffer, blocks_num, &index);
        debug_record_ptr(index);
        printf(inb ? "in\n": "out\n");
    }

    for (i=0; i<151; i++) {
        bool inb = record_ptr_inc(buffer, blocks_num, &index);
        debug_record_ptr(index);
        printf(inb ? "in\n": "out\n");
    }

    printf("--##--");
    index.block_i = blocks_num - 1;
    index.record_i = MAX_RECORDS_PER_BLOCK - 4;

    debug_record_ptr(index);

    for (i=0; i<151; i++) {
        bool inb = record_ptr_inc(buffer, blocks_num, &index);
        debug_record_ptr(index);
        printf(inb ? "in\n": "out\n");
    }

    for (i=0; i<151; i++) {
        bool inb = record_ptr_dec(buffer, blocks_num, &index);
        debug_record_ptr(index);
        printf(inb ? "in\n": "out\n");
    }
}

#endif
