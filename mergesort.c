#include "mergesort.h"
//#include "debug.h"

record_ptr
buffer_quicksort_part(block_t *buffer, unsigned char field, unsigned int blocks_num, record_ptr low, record_ptr high) {
    record_t *pivot = buffer_get_record(buffer, low), *record_i, *record_j;
    record_ptr i = low, j = high;

    record_ptr_dec(buffer, blocks_num, &i);
    record_ptr_inc(buffer, blocks_num, &j);

    while (true) {
        do {
            record_ptr_inc(buffer, blocks_num, &i);
            record_i = buffer_get_record(buffer, i);
        } while (record_cmp(field, record_i, pivot) < 0 && record_ptr_cmp(i, high) < 0);

        do {
            record_ptr_dec(buffer, blocks_num, &j);
            record_j = buffer_get_record(buffer, j);
        } while (record_cmp(field, record_j, pivot) > 0 && record_ptr_cmp(j, low) > 0);

        if (record_ptr_cmp(i, j) >= 0)
            return j;

        record_swap(record_i, record_j);
    }
}

void buffer_quicksort(block_t *buffer, unsigned char field, unsigned int blocks_num, record_ptr low, record_ptr high) {
    if (record_ptr_cmp(low, high) < 0) {
        record_ptr p = buffer_quicksort_part(buffer, field, blocks_num, low, high);
        record_ptr p_plus = p;
        record_ptr_inc(buffer, blocks_num, &p_plus);

        buffer_quicksort(buffer, field, blocks_num, low, p);
        buffer_quicksort(buffer, field, blocks_num, p_plus, high);
    }
}

void merge_segments(unsigned int segments, unsigned char field, block_t *buffer, unsigned int blocks_num, char *outfile,
                    unsigned int *nsorted_segs, unsigned int *nios, unsigned int *nunique) {
    unsigned int segments_curr = segments;
    unsigned int buffer_i, segment_i = 0;
    unsigned int rep, mrg;

    for (rep = 0; true; rep++) {
        segment_i = 0;

        if (nunique != NULL)
            *nunique = 0;

        for (mrg = 0; true; mrg++) {
            // empty buffer and pointers
            char filename_mrg[256];
            filename_segment(filename_mrg, outfile, rep + 1, mrg);
            FILE *file_mrg = fopen(filename_mrg, "wb+");
            ensure(file_mrg != NULL, "Cannot open merge file");

            merge_ptr status[blocks_num];
            buffer_invalidate(buffer, blocks_num);
            buffer_i = 0;
            block_t block_tmp;
            buffer_invalidate(&block_tmp, 1);
            block_tmp.valid = true;

            // load segments
            while (segment_i < segments_curr && buffer_i < blocks_num) {
                char filename[256];
                filename_segment(filename, outfile, rep, segment_i);
                segment_i++;

                if (!file_exists(filename))
                    continue;

                if (!merge_ptr_init(&status[buffer_i], filename, &buffer[buffer_i], 1, nios))
                    continue;

                buffer_i++;
            }

            // sort segments and write to a file
            while (true) {
                record_t selected, previous;
                long long selected_i = -1;
                unsigned int i;
                for (i = 0; i < buffer_i; i++) {
                    if (status[i].buffer_curr_blocks == 0)
                        continue;

                    record_t *current = merge_ptr_get_record(&status[i]);

                    if (selected_i < 0 || record_cmp(field, current, &selected) < 0) {
                        selected = *current;
                        selected_i = i;
                    }
                }

                if (selected_i >= 0) {
                    merge_ptr_inc(&status[selected_i], nios);

                    if (nunique != NULL) {
                        if (*nunique == 0 || record_cmp(field, &selected, &previous) != 0) {
                            block_tmp.entries[block_tmp.nreserved++] = selected;
                            (*nunique)++;
                            previous = selected;
                        }
                    } else {
                        block_tmp.entries[block_tmp.nreserved++] = selected;
                    }

                    if (block_tmp.nreserved >= MAX_RECORDS_PER_BLOCK) {
                        file_write_blocks(file_mrg, &block_tmp, 1, nios);

                        buffer_invalidate(&block_tmp, 1);
                        block_tmp.valid = true;
                    }
                }

                if (selected_i < 0) {
                    unsigned int i;
                    for (i = 0; i < buffer_i; i++) {
                        char filename[256];
                        strcpy(filename, status[i].filename);
                        merge_ptr_clear(&status[i]);
                        file_remove(filename);
                    }

                    if (block_tmp.nreserved > 0)
                        file_write_blocks(file_mrg, &block_tmp, 1, nios);

                    fclose(file_mrg);
                    break;
                }
            }

            if (segment_i >= segments_curr) {
                segments_curr = mrg + 1;
                break;
            }
        }

        if (segments_curr == 1) {
            char filename[256];
            filename_segment(filename, outfile, rep + 1, 0);
            ensure(rename(filename, outfile) == 0, "Cannot rename temp file to outfile");
            break;
        } else {
            // update stats
            (*nsorted_segs) += segments_curr;
        }
    }
}

void merge_sort_ext(char *infile, unsigned char field, block_t *buffer, unsigned int nmem_blocks, char *outfile,
                    unsigned int *nsorted_segs, unsigned int *npasses, unsigned int *nios, unsigned int *nunique) {
    // delete previous output file
    if (file_exists(outfile))
        file_remove(outfile);

    unsigned int nsorted_segs_tmp = 0;
    unsigned int nios_tmp = 0;
    unsigned int nunique_tmp = 0;

    FILE *file;
    file = fopen(infile, "rb");
    ensure(file != NULL, "Cannot open infile");

    // file stats
    unsigned long file_size = file_get_size(file);
    unsigned int file_blocks = file_size / sizeof(block_t);
    ensure(file_blocks > 0, "infile is empty");

    // number of loops needed to sort
    unsigned int buffer_fills = file_blocks / nmem_blocks;
    unsigned int rem_blocks = file_blocks % nmem_blocks;

    // number of total segments for the file
    unsigned int segments = rem_blocks > 0 ? buffer_fills + 1 : buffer_fills;

    unsigned int valid_blocks = nmem_blocks;
    unsigned int i;
    for (i = 0; i <= buffer_fills; i++) {
        // number of blocks to load
        unsigned int buffer_blocks = (i == buffer_fills) ? rem_blocks : nmem_blocks;
        if (buffer_blocks < 1)
            break;

        // empty buffer
        buffer_invalidate(buffer, nmem_blocks);

        file_read_blocks(file, buffer, buffer_blocks, &nios_tmp);
        valid_blocks = buffer_format_complete(buffer, nmem_blocks);

        // skip if there are no valid blocks
        if (valid_blocks <= 0)
            continue;

        // sort buffer using quicksort
        record_ptr low = { .block_i = 0, .record_i = 0 };
        record_ptr high = { .block_i= valid_blocks - 1, .record_i = buffer[valid_blocks - 1].nreserved - 1 };
        buffer_quicksort(buffer, field, valid_blocks, low, high);

        if (nunique != NULL) {
            nunique_tmp = 0; // to keep track of the last unique number in case of one segment
            buffer_eliminate_duplicates(buffer, field, &valid_blocks, &nunique_tmp);
        }

        file_write_buffer(buffer, valid_blocks, i, outfile, &nsorted_segs_tmp, &nios_tmp);
    }

    fclose(file);

    if (segments == 1) {
        char filename_tmp[256];
        filename_segment_buffer(filename_tmp, outfile, 0);

        ensure(file_exists(filename_tmp), "There are no valid records to sort");
        ensure(rename(filename_tmp, outfile) == 0, "Cannot rename temp file to outfile");
    } else {
        nunique_tmp = 0;
        merge_segments(segments, field, buffer, nmem_blocks, outfile, &nsorted_segs_tmp, &nios_tmp, nunique != NULL ? &nunique_tmp: NULL);
    }

    // save stats
    *nios = nios_tmp;

    if (nsorted_segs != NULL)
        *nsorted_segs = nsorted_segs_tmp;

    if (npasses != NULL)
        *npasses = 1 + nsorted_segs_tmp; // the infile is passed only once + each sorted seg is passed once

    if (nunique != NULL)
        *nunique = nunique_tmp;
}

void MergeSort(char *infile, unsigned char field, block_t *buffer, unsigned int nmem_blocks, char *outfile,
               unsigned int *nsorted_segs, unsigned int *npasses, unsigned int *nios) {
    merge_sort_ext(infile, field, buffer, nmem_blocks, outfile, nsorted_segs, npasses, nios, NULL);
}