#ifndef _DBT_UTILS_H
#define _DBT_UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include "dbtproj.h"

#define REC_CMP_RECID   0
#define REC_CMP_NUM     1
#define REC_CMP_STR     2
#define REC_CMP_NUM_STR 3

typedef struct {
    long long block_i;
    long long record_i;
} record_ptr;

typedef struct {
    char filename[128];
    FILE *file;
    unsigned int file_blocks;
    unsigned int file_rem_blocks;

    record_ptr index;
    block_t *buffer;
    unsigned int buffer_max_blocks;
    unsigned int buffer_curr_blocks;
} merge_ptr;

void ensure(bool pass, char *message);

void filename_tmp(char *filename, char *base, char *filename_rel);
void filename_segment(char *filename, char *base, unsigned int rep, unsigned int index);
void filename_hash_bucket(char *filename, char *base, unsigned int index);
void filename_segment_buffer(char *filename, char *base, unsigned int index);

unsigned long file_get_size(FILE *file);
void file_read_blocks(FILE *file, block_t *buffer, unsigned int blocks_num, unsigned int *nios);
void file_write_blocks(FILE *file, block_t *buffer, unsigned int blocks_num, unsigned int *nios);
void file_write_buffer(block_t *buffer, unsigned int blocks_num, unsigned int id, char *outfile, unsigned int *nsorted_segs, unsigned int *nios);
bool file_exists(char *filename);
void file_remove(char *filename);

bool record_ptr_inc(block_t *buffer, int blocks_num, record_ptr *index);
bool record_ptr_dec(block_t *buffer, int blocks_num, record_ptr *index);
int record_ptr_cmp(record_ptr record_a, record_ptr record_b);

void record_swap(record_t *a, record_t *b);
void record_invalidate(record_t *record);
int record_cmp(unsigned char field, record_t *a, record_t *b);

unsigned int buffer_format(block_t *buffer, unsigned int blocks_num);
unsigned int buffer_format_complete(block_t *buffer, unsigned int blocks_num);
record_t *buffer_get_record(block_t *buffer, record_ptr index);
void buffer_invalidate(block_t *buffer, unsigned int blocks_num);
void buffer_eliminate_duplicates(block_t *buffer, unsigned char field, unsigned int *blocks_num, unsigned int *nunique);

void block_swap(block_t *a, block_t *b);
void block_format(block_t *block);

bool merge_ptr_fill(merge_ptr *ptr, unsigned int *nios);
void merge_ptr_clear(merge_ptr *ptr);
void merge_ptr_reset(merge_ptr *ptr, unsigned int *nios);
bool merge_ptr_init(merge_ptr *ptr, char *filename, block_t *buffer, unsigned int blocks_num, unsigned int *nios);
bool merge_ptr_inc(merge_ptr *ptr, unsigned int *nios);
record_t *merge_ptr_get_record(merge_ptr *ptr);

unsigned int hash_int(unsigned int x);
unsigned int hash_record(record_t *record, unsigned char field, unsigned int limit);

#endif